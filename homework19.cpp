﻿#include <iostream>

class Animal
{
public:
    virtual void Voice() const = 0;
};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Dog goes Woof" << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Cat goes Meow" << "\n";
    }
};


class Bird : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Bird goes tweet" << "\n";
    }
};

class Mouse : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "And mouse goes sqeek" << "\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Cow goes moo" << "\n";
    }
};

class Frog : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Frog goes croack" << "\n";
    }
};

class Elephant : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "And elephant goes toot" << "\n";
    }
};

class Ducks : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Ducks say quack" << "\n";
    }
};

class Fish : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "And fish goes blub" << "\n";
    }
};

class Seal : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "And the seal goes ow ow ow" << "\n";
    }
};

class Fox : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "But there is one sound that no one knows: what does the fox say?" << "\n";
    }
};

int main()
{
    Animal* animals[11];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Bird();
    animals[3] = new Mouse();
    animals[4] = new Cow();
    animals[5] = new Frog();
    animals[6] = new Elephant();
    animals[7] = new Ducks();
    animals[8] = new Fish();
    animals[9] = new Seal();
    animals[10] = new Fox();

    for (Animal* a : animals)
        a->Voice();
}
